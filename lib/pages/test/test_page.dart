import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ncov_2019/api/rumor_view_model.dart';
import 'package:ncov_2019/api/protect_view_model.dart';
import 'package:ncov_2019/commom/commom.dart';
import 'package:ncov_2019/widget/card/protect_card.dart';
import 'package:ncov_2019/widget/flutter/my_expansion_tile.dart';
import 'package:ncov_2019/widget/view/title_view.dart';
import 'package:ncov_2019/widget/item/statics.dart';
import 'package:ncov_2019/widget/card/rumor_card.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
class TestPage extends StatefulWidget {
  @override
  _TestPageState createState() => _TestPageState();
}
class _TestPageState extends State<TestPage>
    with AutomaticKeepAliveClientMixin {
  List dataLore = new List();//知识

  bool isReq = false;
  List<dynamic> dataRumor = new List();// 辟谣

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    Notice.addListener(NCOVActions.toTabBarIndex(), (index) {
      if (!listNoEmpty(dataRumor)) getData();
      if (!listNoEmpty(dataLore)) getData();
    });
    getData();
  }
  @override
  bool get wantKeepAlive => true;

  getData() {
    protectViewModel.getData().then((v) {
      setState(() {
        dataLore = v;
        isReq = true;
      });
    });
    rumorListReqViewModel.getRumor().then((v) {
      setState(() {
        dataRumor = v;
        isReq = true;
      });
    });

  }

  Future<void> _refreshData() async {
    final Completer<Null> completer = new Completer<Null>();

    getData();

    new Future.delayed(new Duration(seconds: 2), () {
      completer.complete(null);
      _refreshController.refreshCompleted();
    });

    return completer.future;
  }

  Widget buildItemLore(item) {
    ProtectModel model = item;
    return new ProtectCard(
      model,
      margin: EdgeInsets.only(
        left: 10.0,
        right: 10.0,
        top: model.id == dataLore[0].id ? 0.0 : 0,
        bottom: model.id == dataLore[dataLore.length - 1].id ? 10.0 : 0,
      ),
    );
  }
  Widget buildItemRumor(item) {
    RumorListModel model = item;
    return new RumorCard(
      model,
      margin: EdgeInsets.only(
        left: 20.0,
        right: 20.0,
        top: model.id == dataRumor[0].id ? 20.0 : 10,
        bottom: model.id == dataRumor[dataRumor.length - 1].id ? 20.0 : 10,
      ),
      onTap: () {
        setState(() => model.isOpen = !model.isOpen);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return new Scaffold(
      body: new SmartRefresher(
        controller: _refreshController,
        onRefresh: _refreshData,
        child: new ListView(
          children: <Widget>[
            new Space(),
            new TitleView('辟谣', subTitle: '消息数量：${dataRumor.length}'),
            isReq
                ? listNoEmpty(dataRumor)
                ? new Column(
              children: dataRumor.map(buildItemRumor).toList(),
            )
                : new Center(
              child: new Text(
                '暂无数据',
                style: Theme.of(context).textTheme.display1,
              ),
            )
                : new LoadingView(),
            
            new Space(height: mainSpace / 2),
            new TitleView('防护知识'),
            isReq
                ? listNoEmpty(dataLore)
                ? new Column(children: dataLore.map(buildItemLore).toList())
                : new Center(
              child: new Text(
                '暂无数据',
                style: Theme.of(context).textTheme.display1,
              ),
            )
                : new LoadingView(),

          ]

        )
      ),
    );
  }
  
}